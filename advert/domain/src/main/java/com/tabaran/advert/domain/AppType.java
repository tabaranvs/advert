package com.tabaran.advert.domain;

public enum AppType {
    IOS,
    ANDROID,
    WEBSITE
}
