package com.tabaran.advert.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;
    @Getter @Setter @NonNull
    private String name;
    @Enumerated(EnumType.STRING)
    @Getter @Setter @NonNull
    private AppType type;
    @Getter @Setter @NonNull
    @ElementCollection(targetClass=ContentType.class)
    @Enumerated(EnumType.STRING)
    private Set<ContentType> contentTypes;
    @ManyToOne
    @JoinColumn(name="user_id")
    @Getter @Setter @NonNull
    private User user;
}
