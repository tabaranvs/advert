package com.tabaran.advert.domain;

public enum ContentType {
    VIDEO,
    IMAGE,
    HTML
}
