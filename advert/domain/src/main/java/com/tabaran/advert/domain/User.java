package com.tabaran.advert.domain;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.Set;

@Entity
@RequiredArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;
    @Getter @Setter @NonNull
    private String name;
    @Getter @Setter @NonNull
    @Column(nullable = false, unique = true)
    private String email;
    @Getter @Setter @NonNull
    @Column(nullable = false)
    private String password;
    @Getter @Setter @NonNull
    @Enumerated(EnumType.STRING)
    private UserRole role;
    @OneToMany(mappedBy="user")
    @Cascade({CascadeType.DELETE, CascadeType.SAVE_UPDATE})
    @Getter @Setter
    private Set<Application> applications;

}