package com.tabaran.advert.domain;

public enum UserRole {
    ADMIN,
    ADOPS,
    PUBLISHER;

    private static final String ROLE_PREFIX = "ROLE_";

    @Override
    public String toString() {
        return ROLE_PREFIX + name();
    }
}
