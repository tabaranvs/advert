package com.tabaran.advert.repository;

import com.tabaran.advert.domain.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    List<Application> findByUserId(Long id);
    Optional<Application> findByIdAndUserId(Long id, Long userId);
}
