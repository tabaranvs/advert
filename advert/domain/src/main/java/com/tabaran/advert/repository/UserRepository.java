package com.tabaran.advert.repository;

import com.tabaran.advert.domain.User;
import com.tabaran.advert.domain.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByIdAndRole(Long id, UserRole role);
    Optional<User> findByEmail(String email);
}
