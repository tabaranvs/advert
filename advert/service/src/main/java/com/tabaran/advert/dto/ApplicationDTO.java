package com.tabaran.advert.dto;

import com.tabaran.advert.domain.AppType;
import com.tabaran.advert.domain.ContentType;
import lombok.*;

import java.util.Set;

@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationDTO {

    @Getter @Setter
    private Long id;
    @Getter @Setter @NonNull
    private String name;
    @Getter @Setter @NonNull
    private AppType type;
    @Getter @Setter @NonNull
    private Set<ContentType> contentTypes;
}
