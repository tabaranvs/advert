package com.tabaran.advert.dto;

import lombok.*;

@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class UserDTO {

    private Long id;
    @NonNull
    String name;
    @NonNull
    String email;
    @NonNull
    String password;
}
