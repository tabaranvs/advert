package com.tabaran.advert.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ApplicationNotFoundException extends RuntimeException {

    private final static String APP_NOT_FOUND_BY_ID = "Can't find app with id: %d.";
    private final static String APP_NOT_FOUND_BY_ID_FOR_USER = "Can't find app with id: %d for user with id: %d.";

    public ApplicationNotFoundException(Long id) {
        super(String.format(APP_NOT_FOUND_BY_ID, id));
    }

    public ApplicationNotFoundException(Long id, Long userId) {
        super(String.format(APP_NOT_FOUND_BY_ID_FOR_USER, id, userId));
    }
}
