package com.tabaran.advert.exceptions;

import com.tabaran.advert.domain.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UserNotFoundException extends RuntimeException {

    private static final String USER_NOFOUND_BY_ID = "User with id: %d not found.";
    private static final String USER_NOFOUND_BY_ID_AND_ROLE = "User with id: %d and role: %s not found.";

    public UserNotFoundException(Long userId) {
        super(String.format(USER_NOFOUND_BY_ID, userId));
    }

    public UserNotFoundException(Long userId, UserRole role) {
        super(String.format(USER_NOFOUND_BY_ID_AND_ROLE, userId, role.name()));
    }
}
