package com.tabaran.advert.services;

import com.tabaran.advert.domain.Application;
import com.tabaran.advert.dto.ApplicationDTO;
import com.tabaran.advert.exceptions.ApplicationNotFoundException;
import com.tabaran.advert.exceptions.UserNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tabaran.advert.repository.ApplicationRepository;
import com.tabaran.advert.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApplicationService {

    @Autowired
    private ModelMapper mapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ApplicationRepository applicationRepository;

    public ApplicationService(ApplicationRepository applicationRepository, UserRepository userRepository, ModelMapper mapper) {
        this.applicationRepository = applicationRepository;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    public void createApplication(ApplicationDTO dto, Long userId) throws UserNotFoundException {
        Application application = mapper.map(dto, Application.class);
        application.setUser(userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId)));
        applicationRepository.save(application);
    }

    public List<ApplicationDTO> getApplications() {
        return convertApps(applicationRepository.findAll());
    }

    public List<ApplicationDTO> getApplications(Long userId) {
        return convertApps(applicationRepository.findByUserId(userId));
    }

    public void updateApplication(ApplicationDTO dto) throws ApplicationNotFoundException {
        Application application = applicationRepository.findById(dto.getId())
                .orElseThrow(() -> new ApplicationNotFoundException(dto.getId()));
        mapper.map(dto, application);
        applicationRepository.save(application);
    }

    public void updateApplication(ApplicationDTO dto, Long userId) throws ApplicationNotFoundException {
        Application application = applicationRepository.findByIdAndUserId(dto.getId(), userId)
                .orElseThrow(() -> new ApplicationNotFoundException(dto.getId(), userId));
        mapper.map(dto, application);
        applicationRepository.save(application);
    }

    public void removeApplication(Long id) throws ApplicationNotFoundException {
        Application application = applicationRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException(id));
        applicationRepository.delete(application);
    }

    public void removeApplication(Long appId, Long userId) throws ApplicationNotFoundException {
        Application application = applicationRepository.findByIdAndUserId(appId, userId)
                .orElseThrow(() -> new ApplicationNotFoundException(appId, userId));
        applicationRepository.delete(application);
    }

    private List<ApplicationDTO> convertApps(List<Application> applications) {
        return applications.stream()
                .map(app -> mapper.map(app, ApplicationDTO.class))
                .collect(Collectors.toList());
    }
}
