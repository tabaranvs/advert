package com.tabaran.advert.services;

import com.tabaran.advert.domain.User;
import com.tabaran.advert.domain.UserRole;
import com.tabaran.advert.dto.UserDTO;
import com.tabaran.advert.exceptions.UserNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.tabaran.advert.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {

    private ModelMapper mapper;
    private UserRepository repository;
    private PasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository repository, ModelMapper mapper, PasswordEncoder encoder) {
        this.repository = repository;
        this.mapper = mapper;
        this.encoder = encoder;
    }

    public List<UserDTO> getAll() {
        return repository.findAll().stream()
                .map(user -> mapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    public UserDTO getUser(Long id) throws UserNotFoundException {
        return mapper.map(repository.findById(id).orElseThrow(() -> new UserNotFoundException(id)), UserDTO.class);
    }

    public Long createUser(UserDTO dto, UserRole role) {
        User user = mapper.map(dto, User.class);
        user.setRole(role);
        user.setPassword(encoder.encode(dto.getPassword()));
        return repository.save(user).getId();
    }

    public void updateUser(UserDTO dto, UserRole role) throws UserNotFoundException {
        User user = repository.findByIdAndRole(dto.getId(), role)
                .orElseThrow(() -> new UserNotFoundException(dto.getId(), role));
        mapper.map(dto, user);
        user.setPassword(encoder.encode(dto.getPassword()));
        repository.save(user);
    }

    public void removeUser(Long id, UserRole role) throws UserNotFoundException {
        repository.delete(repository.findByIdAndRole(id, role)
                .orElseThrow(() -> new UserNotFoundException(id, role)));
    }
}
