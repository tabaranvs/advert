package com.tabaran.advert.services;

import com.tabaran.advert.domain.AppType;
import com.tabaran.advert.domain.Application;
import com.tabaran.advert.domain.User;
import com.tabaran.advert.dto.ApplicationDTO;
import com.tabaran.advert.exceptions.ApplicationNotFoundException;
import com.tabaran.advert.exceptions.UserNotFoundException;
import com.tabaran.advert.services.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.tabaran.advert.repository.ApplicationRepository;
import com.tabaran.advert.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ApplicationServiceTest {

    @Mock
    private ApplicationRepository applicationRepository;

    @Mock
    private UserRepository userRepository;

    @Spy
    private ModelMapper mapper;

    @InjectMocks
    private ApplicationService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createApplicationTest() throws UserNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        Application createdApp = TestUtils.createApplication(TestUtils.APP_ID, TestUtils.USER_ID);
        ArgumentCaptor<Application> appArg = ArgumentCaptor.forClass(Application.class);
        User user = TestUtils.createUser(TestUtils.USER_ID);
        when(userRepository.findById(TestUtils.USER_ID)).thenReturn(of(user));
        when(applicationRepository.findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID)).thenReturn(of(createdApp));

        service.createApplication(dto, TestUtils.USER_ID);

        verify(userRepository, times(1)).findById(TestUtils.USER_ID);
        verify(applicationRepository, times(1)).save(appArg.capture());
        TestUtils.checkIfAppModelEqualsToDTO(appArg.getValue(), dto, user);
    }

    @Test(expected = UserNotFoundException.class)
    public void createApplicationByNonExistingUserTest() throws UserNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        when(userRepository.findById(TestUtils.USER_ID)).thenReturn(empty());

        service.createApplication(dto, TestUtils.USER_ID);
    }

    @Test
    public void getApplicationsTest() {
        int appCount = 5;
        List<Application> apps = LongStream.range(0, appCount).mapToObj(index -> TestUtils.createApplication(index, TestUtils.USER_ID))
                .collect(Collectors.toList());
        User user = TestUtils.createUser((int) TestUtils.USER_ID);
        when(applicationRepository.findByUserId(TestUtils.USER_ID)).thenReturn(apps);

        List<ApplicationDTO> result = service.getApplications(TestUtils.USER_ID);

        assertEquals(result.size(), apps.size());
        IntStream.range(0, appCount).forEach(index ->
                TestUtils.checkIfAppModelEqualsToDTO(apps.get(index), result.get(index), user));
    }

    @Test
    public void updateApplicationTest() throws ApplicationNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_ID, TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        Application app = TestUtils.createApplication(TestUtils.APP_ID, TestUtils.USER_ID);
        ArgumentCaptor<Application> appArg = ArgumentCaptor.forClass(Application.class);
        when(applicationRepository.findById(TestUtils.APP_ID)).thenReturn(of(app));

        service.updateApplication(dto);

        verify(applicationRepository, times(1)).findById(TestUtils.APP_ID);
        verify(applicationRepository, times(1)).save(appArg.capture());
        TestUtils.checkIfAppModelEqualsToDTO(app, dto, TestUtils.createUser(TestUtils.USER_ID));
    }

    @Test(expected = ApplicationNotFoundException.class)
    public void updateApplicationIncorrectIdTest() throws ApplicationNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_ID, TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        when(applicationRepository.findById(TestUtils.APP_ID)).thenReturn(empty());

        service.updateApplication(dto);
    }

    @Test
    public void updateUsersApplicationTest() throws ApplicationNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_ID, TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        Application app = TestUtils.createApplication(TestUtils.APP_ID, TestUtils.USER_ID);
        ArgumentCaptor<Application> appArg = ArgumentCaptor.forClass(Application.class);
        when(applicationRepository.findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID)).thenReturn(of(app));

        service.updateApplication(dto, TestUtils.USER_ID);

        verify(applicationRepository, times(1)).findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID);
        verify(applicationRepository, times(1)).save(appArg.capture());
        TestUtils.checkIfAppModelEqualsToDTO(app, dto, TestUtils.createUser(TestUtils.USER_ID));
    }

    @Test(expected = ApplicationNotFoundException.class)
    public void updateUsersApplicationIncorrectUserIdTest() throws ApplicationNotFoundException {
        ApplicationDTO dto = new ApplicationDTO(TestUtils.APP_ID, TestUtils.APP_NAME, AppType.IOS, TestUtils.createFullSetOfContentTypes());
        when(applicationRepository.findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID)).thenReturn(empty());

        service.updateApplication(dto, TestUtils.USER_ID);
    }

    @Test
    public void removeApplicationTest() throws ApplicationNotFoundException {
        Application app = TestUtils.createApplication(TestUtils.APP_ID, TestUtils.USER_ID);
        when(applicationRepository.findById(TestUtils.APP_ID)).thenReturn(of(app));

        service.removeApplication(TestUtils.APP_ID);

        verify(applicationRepository, times(1)).findById(TestUtils.APP_ID);
        verify(applicationRepository, times(1)).delete(app);
    }

    @Test(expected = ApplicationNotFoundException.class)
    public void removeApplicationIncorrectIdTest() throws ApplicationNotFoundException {
        when(applicationRepository.findById(TestUtils.APP_ID)).thenReturn(empty());
        service.removeApplication(TestUtils.APP_ID);
    }

    @Test
    public void removeUsersApplicationTest() throws ApplicationNotFoundException {
        Application app = TestUtils.createApplication(TestUtils.APP_ID, TestUtils.USER_ID);
        when(applicationRepository.findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID)).thenReturn(of(app));

        service.removeApplication(TestUtils.APP_ID, TestUtils.USER_ID);

        verify(applicationRepository, times(1)).findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID);
        verify(applicationRepository, times(1)).delete(app);
    }

    @Test(expected = ApplicationNotFoundException.class)
    public void removeUsersApplicationIncorrectIdTest() throws ApplicationNotFoundException {
        when(applicationRepository.findByIdAndUserId(TestUtils.APP_ID, TestUtils.USER_ID)).thenReturn(empty());
        service.removeApplication(TestUtils.APP_ID, TestUtils.USER_ID);
    }
}
