package com.tabaran.advert.services;

import com.tabaran.advert.domain.User;
import com.tabaran.advert.domain.UserRole;
import com.tabaran.advert.dto.UserDTO;
import com.tabaran.advert.exceptions.UserNotFoundException;
import com.tabaran.advert.services.utils.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.tabaran.advert.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.tabaran.advert.services.utils.TestUtils.USER_ID;
import static com.tabaran.advert.services.utils.TestUtils.createUser;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Spy
    private ModelMapper mapper = new ModelMapper();

    @Mock
    private UserRepository repository;

    @Mock
    private PasswordEncoder encoder;

    @InjectMocks
    private UserService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getAllTest() {
        int usersCount = 5;
        List<User> storedUsers = IntStream.range(0, usersCount).mapToObj(TestUtils::createUser)
                .collect(Collectors.toList());
        when(repository.findAll()).thenReturn(storedUsers);

        List<UserDTO> users = service.getAll();

        verify(repository, times(1)).findAll();

        assertEquals(users.size(), usersCount);
        IntStream.range(0, storedUsers.size()).forEach(index ->
                TestUtils.checkIfUserModelEqualsToDTO(storedUsers.get(index), users.get(index))
        );
    }

    @Test
    public void getUserTest() throws UserNotFoundException {
        User userMock = createUser((int) TestUtils.USER_ID);
        ArgumentCaptor<Long> idArg = ArgumentCaptor.forClass(Long.class);
        when(repository.findById(TestUtils.USER_ID)).thenReturn(of(userMock));

        UserDTO userDTO = service.getUser(TestUtils.USER_ID);

        verify(repository, times(1)).findById(idArg.capture());

        Assert.assertEquals(TestUtils.USER_ID, (long) idArg.getValue());
        TestUtils.checkIfUserModelEqualsToDTO(userMock, userDTO);
    }

    @Test(expected = UserNotFoundException.class)
    public void getNotExistingUserTest() throws UserNotFoundException {
        when(repository.findById(anyLong())).thenReturn(empty());
        service.getUser(0L);
    }

    @Test
    public void createUserTest() {
        ArgumentCaptor<User> userArg = ArgumentCaptor.forClass(User.class);
        UserDTO dto = new UserDTO(TestUtils.USER_NAME, String.format(TestUtils.EMAIL_PATTERN, 0), TestUtils.USER_PASSWORD);
        when(encoder.encode(anyString())).thenReturn(dto.getPassword());
        when(repository.save(any())).thenReturn(createUser(USER_ID));

        assertEquals(service.createUser(dto, UserRole.ADOPS), Long.valueOf(USER_ID));

        verify(repository, times(1)).save(userArg.capture());
        verify(encoder, times(1)).encode(dto.getPassword());

        User savedUser = userArg.getValue();
        TestUtils.checkIfUserModelEqualsToDTO(savedUser, dto);
        assertEquals(UserRole.ADOPS, savedUser.getRole());
    }

    @Test(expected = MappingException.class)
    public void createUserTestWithEmptyDtoTest() {
        UserDTO dto = new UserDTO();
        service.createUser(dto, UserRole.ADOPS);
        verify(repository, times(0)).save(any());
        verify(encoder, times(0)).encode(dto.getPassword());
    }

    @Test
    public void updateUserTest() throws UserNotFoundException, NoSuchFieldException, IllegalAccessException {
        UserRole role = UserRole.ADMIN;
        String updated = "updated";
        UserDTO dto = new UserDTO(TestUtils.USER_ID, TestUtils.USER_NAME + updated, String.format(TestUtils.EMAIL_PATTERN, updated), TestUtils.USER_PASSWORD + updated);
        ArgumentCaptor<User> userArg = ArgumentCaptor.forClass(User.class);
        User user = mapper.map(dto, User.class);
        TestUtils.forceSetId(user, TestUtils.USER_ID);
        when(repository.findByIdAndRole(TestUtils.USER_ID, role)).thenReturn(of(user));
        when(encoder.encode(dto.getPassword())).thenReturn(dto.getPassword());

        service.updateUser(dto, role);

        verify(repository, times(1)).findByIdAndRole(TestUtils.USER_ID, role);
        verify(encoder, times(1)).encode(dto.getPassword());
        verify(repository, times(1)).save(userArg.capture());

        TestUtils.checkIfUserModelEqualsToDTO(userArg.getValue(), dto);
    }

    @Test(expected = UserNotFoundException.class)
    public void updateUserWithIncorrectParamsTest() throws UserNotFoundException {
        when(repository.findByIdAndRole(anyLong(), any(UserRole.class))).thenReturn(empty());
        service.updateUser(new UserDTO(), UserRole.ADMIN);
    }

    @Test
    public void removeUserTest() throws UserNotFoundException {
        User user = createUser((int) TestUtils.USER_ID);
        when(repository.findByIdAndRole(TestUtils.USER_ID, UserRole.ADMIN)).thenReturn(of(user));

        service.removeUser(TestUtils.USER_ID, UserRole.ADMIN);

        verify(repository, times(1)).findByIdAndRole(TestUtils.USER_ID, UserRole.ADMIN);
        verify(repository, times(1)).delete(user);
    }

    @Test(expected = UserNotFoundException.class)
    public void removeUserNotExistingTest() throws UserNotFoundException {
        when(repository.findByIdAndRole(anyLong(), any(UserRole.class))).thenReturn(empty());
        service.removeUser(TestUtils.USER_ID, UserRole.ADMIN);
    }
}
