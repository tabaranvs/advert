package com.tabaran.advert.services.utils;

import com.tabaran.advert.domain.*;
import com.tabaran.advert.dto.ApplicationDTO;
import com.tabaran.advert.dto.UserDTO;
import org.assertj.core.util.Sets;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestUtils {

    public static final long USER_ID = 0L;
    public static final long APP_ID = 0L;
    public static final String EMAIL_PATTERN = "email%s@advrt.com";
    public static final String USER_NAME = "name";
    public static final String USER_PASSWORD = "pass";
    public static final String APP_NAME = "app";

    public static User createUser(long userId) {
        User user = new User(USER_NAME + userId,
                String.format(EMAIL_PATTERN, userId),
                USER_PASSWORD + userId,
                UserRole.ADMIN);
        forceSetId(user, userId);
        return user;
    }

    public static Application createApplication(long appId, long userId) {
        Application application = new Application(APP_NAME + appId, AppType.IOS, createFullSetOfContentTypes(), createUser(userId));
        forceSetId(application, appId);
        return application;
    }

    public static Set<ContentType> createFullSetOfContentTypes() {
        return Sets.newHashSet(Arrays.asList(ContentType.values()));
    }

    public static void forceSetId(Object obj, Long userId) {
        try {
            Field idField  = obj.getClass().getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(obj, userId);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void checkIfUserModelEqualsToDTO(User user, UserDTO dto) {
        assertEquals(dto.getId(), user.getId());
        assertEquals(dto.getName(), user.getName());
        assertEquals(dto.getEmail(), user.getEmail());
        assertEquals(dto.getPassword(), user.getPassword());
    }

    public static void checkIfAppModelEqualsToDTO(Application app, ApplicationDTO appDTO, User user) {
        assertEquals(app.getId(), appDTO.getId());
        assertEquals(app.getName(), appDTO.getName());
        assertEquals(app.getType(), appDTO.getType());
        assertEquals(app.getContentTypes().size(), appDTO.getContentTypes().size());
        app.getContentTypes().forEach(type ->
                assertTrue(appDTO.getContentTypes().contains(type)));
        assertEquals(app.getUser().getId(), user.getId());
        assertEquals(app.getUser().getName(), user.getName());
        assertEquals(app.getUser().getEmail(), user.getEmail());
        assertEquals(app.getUser().getPassword(), user.getPassword());
        assertEquals(app.getUser().getRole(), user.getRole());
    }
}
