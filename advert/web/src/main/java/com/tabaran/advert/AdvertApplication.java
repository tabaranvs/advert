package com.tabaran.advert;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EntityScan(basePackages = {"com.tabaran.advert.domain"})
@EnableJpaRepositories(basePackages = "com.tabaran.advert.repository")
@ComponentScan(basePackages = {"com.tabaran.advert"})
public class AdvertApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvertApplication.class, args);
    }

    @Bean
    public ModelMapper getMapper() {
        return new ModelMapper();
    }
}

