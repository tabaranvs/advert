package com.tabaran.advert.controller;

import com.tabaran.advert.dto.ApplicationDTO;
import com.tabaran.advert.security.UserDetails;
import com.tabaran.advert.services.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Secured("ROLE_ADOPS")
@RequestMapping("/applications")
public class ApplicationController {

    private ApplicationService applicationService;

    @Autowired
    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping("/all")
    public List<ApplicationDTO> retrieveAll() {
        return applicationService.getApplications();
    }

    @Secured({"ROLE_ADOPS", "ROLE_PUBLISHER"})
    @GetMapping("/my")
    public List<ApplicationDTO> retrieveMy() {
        return applicationService.getApplications(getCurrentUser().getUserId());
    }

    @Secured({"ROLE_ADOPS", "ROLE_PUBLISHER"})
    @PostMapping
    public void createApplication(@RequestBody ApplicationDTO dto) {
        applicationService.createApplication(dto, getCurrentUser().getUserId());
    }

    @PutMapping
    public void updateApplication(@RequestBody ApplicationDTO dto) {
        applicationService.updateApplication(dto);
    }

    @Secured({"ROLE_ADOPS", "ROLE_PUBLISHER"})
    @PutMapping("/my")
    public void updateMyApplication(@RequestBody ApplicationDTO dto) {
        applicationService.updateApplication(dto, getCurrentUser().getUserId());
    }

    @DeleteMapping
    public void removeApplication(@RequestParam Long id) {
        applicationService.removeApplication(id);
    }

    @Secured({"ROLE_ADOPS", "ROLE_PUBLISHER"})
    @DeleteMapping("/my")
    public void removeMyApplication(@RequestParam Long id) {
        applicationService.removeApplication(id, getCurrentUser().getUserId());
    }

    private UserDetails getCurrentUser() {
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
