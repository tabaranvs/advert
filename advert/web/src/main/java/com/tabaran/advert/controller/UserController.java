package com.tabaran.advert.controller;

import com.tabaran.advert.domain.UserRole;
import com.tabaran.advert.dto.UserDTO;
import com.tabaran.advert.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Secured("ROLE_ADMIN")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<UserDTO> retrieveAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public void retrieveUser(@PathVariable Long id) {
        userService.getUser(id);
    }

    @Secured({"ROLE_ADOPS", "ROLE_ADMIN"})
    @PostMapping("/publishers")
    public Long createPublisher(@RequestBody UserDTO user) {
        return userService.createUser(user, UserRole.PUBLISHER);
    }

    @Secured({"ROLE_ADOPS", "ROLE_ADMIN"})
    @PutMapping("/publishers")
    public void updatePublisher(@RequestBody UserDTO user) {
        userService.updateUser(user, UserRole.PUBLISHER);
    }

    @Secured({"ROLE_ADOPS", "ROLE_ADMIN"})
    @DeleteMapping("/publishers")
    public void removePublisher(@RequestParam Long id) {
        userService.removeUser(id, UserRole.PUBLISHER);
    }

    @PostMapping("/operators")
    public Long createOperator(@RequestBody UserDTO user) {
        return userService.createUser(user, UserRole.ADOPS);
    }

    @PutMapping("/operators")
    public void updateOperator(@RequestBody UserDTO user) {
        userService.updateUser(user, UserRole.ADOPS);
    }

    @DeleteMapping("/operators")
    public void removeOperator(@RequestParam Long id) {
        userService.removeUser(id, UserRole.ADOPS);
    }
}
