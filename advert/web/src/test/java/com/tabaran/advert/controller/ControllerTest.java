package com.tabaran.advert.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tabaran.advert.AdvertApplication;
import com.tabaran.advert.SecurityConfig;
import com.tabaran.advert.domain.AppType;
import com.tabaran.advert.domain.ContentType;
import com.tabaran.advert.domain.UserRole;
import com.tabaran.advert.dto.ApplicationDTO;
import com.tabaran.advert.dto.UserDTO;
import com.tabaran.advert.exceptions.UserNotFoundException;
import com.tabaran.advert.security.UserDetails;
import com.tabaran.advert.services.ApplicationService;
import com.tabaran.advert.services.UserService;
import org.assertj.core.util.Sets;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static com.tabaran.advert.domain.UserRole.*;
import static org.junit.runners.Parameterized.Parameters;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(Parameterized.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SecurityConfig.class, AdvertApplication.class})
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ControllerTest {
    private MockMvc mockMvc;

    private static final String USERS_URL = "/users";
    private static final String APP_URL = "/applications";

    private static final String USER_NAME = "user";

    private static final long ADMIN_ID = 0L;
    private static final long PUBLISHER_ID_1 = 1L;
    private static final long PUBLISHER_ID_2 = 2L;
    private static final long OPERATOR_ID_1 = 3L;
    private static final long OPERATOR_ID_2 = 4L;

    private static final long APP_ID_1 = 5L;
    private static final long APP_ID_2 = 6L;


    private static final ObjectMapper JSONmapper = new ObjectMapper();
    private static final Set<ContentType> contentTypes = Sets.newHashSet(Arrays.asList(ContentType.values()));


    private MockHttpServletRequestBuilder request;
    private ResultMatcher expectedStatus;

    public ControllerTest(String name, MockHttpServletRequestBuilder request, ResultMatcher expected) {
        this.request = request;
        this.expectedStatus = expected;
    }

    @Parameters(name = "{index}: {0};")
    public static List<Object[]> data() throws JsonProcessingException {
        return Arrays.asList(new Object[][]{
                //---------------------------------UsersController tests---------------------------------------
                {"Get all users " + ADMIN,
                        get(USERS_URL + "/all").with(user(USER_NAME).roles(ADMIN.name())),
                        status().isOk()},
                {"Get all users " + ADOPS,
                        get(USERS_URL + "/all").with(user(USER_NAME).roles(ADOPS.name())),
                        status().is(FORBIDDEN.value())},
                {"Get all users " + PUBLISHER,
                        get(USERS_URL + "/all").with(user(USER_NAME).roles(PUBLISHER.name())),
                        status().is(FORBIDDEN.value())},

                {"Get user by id " + ADMIN,
                        get(USERS_URL + "/" + ADMIN_ID).with(user(USER_NAME).roles(ADMIN.name())),
                        status().isOk()},
                {"Get user by id " + ADOPS,
                        get(USERS_URL + "/" + ADMIN_ID).with(user(USER_NAME).roles(ADOPS.name())),
                        status().is(FORBIDDEN.value())},
                {"Get user by id " + PUBLISHER,
                        get(USERS_URL + "/" + ADMIN_ID).with(user(USER_NAME).roles(PUBLISHER.name())),
                        status().is(FORBIDDEN.value())},


                {"Create publisher " + ADMIN,
                        post(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADMIN.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().isOk()},
                {"Create publisher " + ADOPS,
                        post(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADOPS.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().isOk()},
                {"Create publisher " + PUBLISHER,
                        post(USERS_URL + "/publishers").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(FORBIDDEN.value())},

                {"Edit publisher " + ADMIN, put(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADMIN.name()))
                        .contentType(APPLICATION_JSON).content(createUserJSON(PUBLISHER_ID_1)),
                        status().isOk()},
                {"Edit publisher " + ADOPS,
                        put(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADOPS.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(PUBLISHER_ID_1)),
                        status().isOk()},
                {"Edit publisher " + ADOPS,
                        put(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADOPS.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)), status().is(BAD_REQUEST.value())},
                {"Edit publisher " + PUBLISHER,
                        put(USERS_URL + "/publishers").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(PUBLISHER_ID_1)),
                        status().is(FORBIDDEN.value())},

                {"Remove publisher " + ADMIN,
                        delete(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADMIN.name()))
                                .param("id", String.valueOf(PUBLISHER_ID_1)),
                        status().isOk()},
                {"Remove publisher " + ADOPS,
                        delete(USERS_URL + "/publishers").with(user(USER_NAME).roles(ADOPS.name()))
                                .param("id", String.valueOf(PUBLISHER_ID_2)),
                        status().isOk()},
                {"Remove publisher " + PUBLISHER,
                        delete(USERS_URL + "/publishers").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .param("id", "1"),
                        status().is(FORBIDDEN.value())},

                {"Create operator " + ADMIN,
                        post(USERS_URL + "/operators").with(user(USER_NAME).roles(ADMIN.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().isOk()},
                {"Create operator " + ADOPS,
                        post(USERS_URL + "/operators").with(user(USER_NAME).roles(ADOPS.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(FORBIDDEN.value())},
                {"Create operator " + PUBLISHER,
                        post(USERS_URL + "/operators").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(FORBIDDEN.value())},

                {"Edit operator " + ADMIN,
                        put(USERS_URL + "/operators").with(user(USER_NAME).roles(ADMIN.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(OPERATOR_ID_1)),
                        status().isOk()},
                {"Edit operator with incorrect request params " + ADMIN,
                        put(USERS_URL + "/operators").with(user(USER_NAME).roles(ADMIN.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(BAD_REQUEST.value())},
                {"Edit operator " + ADOPS,
                        put(USERS_URL + "/operators").with(user(USER_NAME).roles(ADOPS.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(FORBIDDEN.value())},
                {"Edit operator " + PUBLISHER,
                        put(USERS_URL + "/operators").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .contentType(APPLICATION_JSON).content(createUserJSON(null)),
                        status().is(FORBIDDEN.value())},


                {"Remove operator " + ADMIN,
                        delete(USERS_URL + "/operators").with(user(USER_NAME).roles(ADMIN.name()))
                                .param("id", String.valueOf(OPERATOR_ID_1)),
                        status().isOk()},
                {"Remove operator " + ADOPS,
                        delete(USERS_URL + "/operators").with(user(USER_NAME).roles(ADOPS.name()))
                                .param("id", String.valueOf(OPERATOR_ID_2)),
                        status().is(FORBIDDEN.value())},
                {"Remove operator " + PUBLISHER,
                        delete(USERS_URL + "/operators").with(user(USER_NAME).roles(PUBLISHER.name()))
                                .param("id", String.valueOf(OPERATOR_ID_2)),
                        status().is(FORBIDDEN.value())},

                //-----------------------------------ApplicationsController tests-------------------------------------
                {"Get all applications " + ADMIN,
                        get(APP_URL + "/all").with(user(USER_NAME).roles(ADMIN.name())),
                        status().is(FORBIDDEN.value())},
                {"Get all applications " + ADOPS,
                        get(APP_URL + "/all").with(user(USER_NAME).roles(ADOPS.name())),
                        status().isOk()},
                {"Get all applications " + PUBLISHER,
                        get(APP_URL + "/all").with(user(USER_NAME).roles(PUBLISHER.name())),
                        status().is(FORBIDDEN.value())},

                //Get users applications
                {"Get users applications " + ADMIN,
                        get(APP_URL + "/my").with(user(USER_NAME).roles(ADMIN.name())),
                        status().is(FORBIDDEN.value())},
                {"Get users applications " + PUBLISHER,
                        get(APP_URL + "/my").with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER))),
                        status().isOk()},
                {"Get users applications " + ADOPS,
                        get(APP_URL + "/my").with(user(createUserDetails(OPERATOR_ID_1, ADOPS))),
                        status().isOk()},

                {"Create application " + PUBLISHER,
                        post(APP_URL).with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(null)),
                        status().isOk()},
                {"Create application " + ADOPS,
                        post(APP_URL).with(user(createUserDetails(OPERATOR_ID_1, ADOPS)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(null)),
                        status().isOk()},
                {"Create application " + ADMIN,
                        post(APP_URL).with(user(createUserDetails(ADMIN_ID, ADMIN)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(null)),
                        status().is(FORBIDDEN.value())},

                {"Update application " + PUBLISHER,
                        put(APP_URL).with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_1)),
                        status().is(FORBIDDEN.value())},
                {"Update application " + ADOPS,
                        put(APP_URL).with(user(createUserDetails(OPERATOR_ID_1, ADOPS)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_1)),
                        status().isOk()},
                {"Update application " + ADMIN,
                        put(APP_URL).with(user(createUserDetails(ADMIN_ID, ADMIN)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_1)),
                        status().is(FORBIDDEN.value())},

                {"Update users application " + PUBLISHER,
                        put(APP_URL + "/my").with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_1)),
                        status().isOk()},
                {"Update users application " + ADOPS,
                        put(APP_URL + "/my").with(user(createUserDetails(OPERATOR_ID_1, ADOPS)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_2)),
                        status().isOk()},
                {"Update users application " + ADMIN,
                        put(APP_URL + "/my").with(user(createUserDetails(ADMIN_ID, ADMIN)))
                                .contentType(APPLICATION_JSON).content(createAppJSON(APP_ID_1)),
                        status().is(FORBIDDEN.value())},

                {"Remove application " + ADMIN,
                        delete(APP_URL).with(user(USER_NAME).roles(ADMIN.name())).param("id", String.valueOf(APP_ID_1)),
                        status().is(FORBIDDEN.value())},
                {"Remove application " + ADOPS,
                        delete(APP_URL).with(user(USER_NAME).roles(ADOPS.name())).param("id", String.valueOf(APP_ID_1)),
                        status().isOk()},
                {"Remove application " + PUBLISHER,
                        delete(APP_URL).with(user(USER_NAME).roles(PUBLISHER.name())).param("id", String.valueOf(APP_ID_1)),
                        status().is(FORBIDDEN.value())},

                {"Remove users application " + ADMIN,
                        delete(APP_URL + "/my").with(user(createUserDetails(ADMIN_ID, ADMIN)))
                                .param("id", String.valueOf(APP_ID_1)),
                        status().is(FORBIDDEN.value())},
                {"Remove users application " + PUBLISHER,
                        delete(APP_URL + "/my").with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER)))
                                .param("id", String.valueOf(APP_ID_1)),
                        status().isOk()},
                {"Remove users application " + PUBLISHER,
                        delete(APP_URL + "/my").with(user(createUserDetails(PUBLISHER_ID_1, PUBLISHER)))
                                .param("id", String.valueOf(APP_ID_2)),
                        status().is(BAD_REQUEST.value())},
                {"Remove users application " + ADOPS,
                        delete(APP_URL + "/my").with(user(createUserDetails(OPERATOR_ID_1, ADOPS)))
                                .param("id", String.valueOf(APP_ID_2)),
                        status().isOk()}
        });
    }

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationService applicationService;

    @Before
    public final void initContext() throws UserNotFoundException {
        mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();

        userService.createUser(new UserDTO("publisher1", "publisher1@advert.com", "pass"), PUBLISHER);
        userService.createUser(new UserDTO("publisher2", "publisher2@advert.com", "pass"), PUBLISHER);
        userService.createUser(new UserDTO("operator1", "operator1@advert.com", "pass"), ADOPS);
        userService.createUser(new UserDTO("operator2", "operator2@advert.com", "pass"), ADOPS);
        applicationService.createApplication(new ApplicationDTO("app1", AppType.IOS, contentTypes), PUBLISHER_ID_1);
        applicationService.createApplication(new ApplicationDTO("app2", AppType.ANDROID, contentTypes), OPERATOR_ID_1);
    }

    @Test
    public void controllerTest() throws Exception {
        mockMvc.perform(request).andExpect(expectedStatus);
    }

    private static String createUserJSON(Long id) throws JsonProcessingException {
        UserDTO dto = new UserDTO(USER_NAME, UUID.randomUUID().toString() + "@advert.com", "pass");
        if (id != null) {
            dto.setId(id);
        }
        return JSONmapper.writeValueAsString(dto);
    }

    private static String createAppJSON(Long id) throws JsonProcessingException {
        ApplicationDTO dto = new ApplicationDTO("app1", AppType.IOS, contentTypes);
        if (id != null) {
            dto.setId(id);
        }
        return JSONmapper.writeValueAsString(dto);
    }

    private static UserDetails createUserDetails(long id, UserRole role) {
        return new com.tabaran.advert.security.UserDetails(id, USER_NAME, "pass",
                Collections.singletonList(new SimpleGrantedAuthority(role.toString())));
    }

}
